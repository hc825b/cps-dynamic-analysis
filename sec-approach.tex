\section{Our Approach}\label{sec:approach}

To help conduct statistical dynamic analysis over \Koord program, we design
extensions to original \Koord language, which are new data types to indicate
uncertain data sources. We first describe the syntax and semantics of uncertain
types, especially arithmetic operations over these type with base numeric types.
We next explain how we can extend the existing simulation framework for \Koord
program to support uncertain types.

\subsection{Uncertain Types}

Inspired by \UncertainT paper~\cite{bornholt_uncertain<t>:_2014}, we introduced
uncertain types and the type system for data with uncertainty. In \UncertainT
paper, they proposed a general methodology to implement uncertain version of a
given type. Our approach simplifies the proposed technique in \UncertainT.
Instead of using generic types, we directly introduce additional data types
that are essentially uncertain versions of primitive data types such as {\tt
boolean}, {\tt int}, and {\tt float}. In this section, we illustrate our
approach through a simplified version of \Koord.

\paragraph{Programming Language}
The program syntax of simplified \Koord is shown in Figure~\ref{fig:syntax}.
A program is declared with a key word {\tt agent}, and it has its own sensor
and actuator ports to communicate with environment. Note that sensors and
actuators can refer to not only hardware but also input and output to
sub-components interacting with hardware such as state estimation algorithms.
Each agent can also declare local variables and specify an initial set of
states via the $Init$ expression.

The primitive types in \Koord includes {\tt int}, {\tt float}, {\tt bool}, and
a special type {\tt Position} whose value denotes a position in 2- or
3-dimensional map. We only discuss 2-D in this work for simplicity.
We exclude non-numeric types, such as {\tt char} and {\tt string}, that are
irrelevant to sensors or otherwise always precise. The additional types
introduce by {\tt uncertain} keyword are uncertain version of the numeric
types. Loops and functions are considered as syntax sugars because
recursion is not allowed and all loops are constant-bounded in \Koord, and we
skip them here for simplicity.

Observing our syntax, one might question the practicality of a simple language
with only numerical datatype and constant bounded loop. This is from our
observation that unbounded data types, such as variable-length arrays, lists, or
trees, are seldom used to store noisy sensor readings. Therefore, constant
bounded loop should suffice for one discrete transition.

\begin{figure}[ht]
    \grammarindent3.5em
    \grammarparsep4pt
    \begin{grammar}
    % Not printing angle bracket for Non-terminals
    \renewcommand{\syntleft}{\normalfont\itshape}
    \renewcommand{\syntright}{}

    <Program> ::= "agent" <Ports> <Local> <Init> <Event*>

    <Ports> ::= "sensor" <Decl*> "actuator" <Decl*>

    <Local> ::= "local" <Decl*>

    <Decl> ::= <Type> <Var>

    <Init> ::= "init" <Expr>

    <Event> ::= <identifier> ": pre " <Expr> " eff " <Stmt*> ""

    <Type> ::= <NType> | "uncertain" <NType>

    <NType> ::= "int" | "float" | "bool" | "Position"

    <Var> ::= <identifier>

    <Val> ::= $\mathbb{Z}$ | $\mathbb{Q}$ | "true" | "false"
            | "Pos" $(\mathbb{Z}, \mathbb{Z})$
          % | "Pos3D" $(\mathbb{Z}, \mathbb{Z}, \mathbb{Z})$

    <Expr> ::= <Val> | <Var> | "posX(" <Expr> ")" | "posY(" <Expr> ")"
          \alt <Expr> $\oplus$ <Expr> | <Expr> $\bowtie$ <Expr>
          \alt <Expr> "==" <Expr> | <Expr> "!=" <Expr>
          \alt "!" <Expr> | <Expr> $\diamondsuit$ <Expr>

    <Stmt> ::= <Var> = <Expr>
             | "if" <Expr> ":" <Stmt*> "else :" <Stmt*>

    <\(\oplus\)> ::= "+" | "-" | "*" | "/"

    <\(\bowtie\)> ::= ">=" | "<=" | ">" | "<"

    <\(\diamondsuit\)> ::= "\&\&" | "||"

    \end{grammar}
    \caption{Syntax}\label{fig:syntax}
\end{figure}


\paragraph{Type rules}

\newcommand{\Env}{\ensuremath{^{s}\Gamma}\xspace}
\newcommand{\uncertain}[1]{{\tt uncertain\ #1}}
\newcommand{\posX}{{\tt posX}}
\newcommand{\posY}{{\tt posY}}

Since the language is static typed, the type of each variable is known after
declaration; therefore we only discuss type rules for expressions.
Given the static type environment \Env maps variables to their
declared types, the expressions are typed as follows. Notice that there is no
type rule for (in)equality operators for uncertain values, so comparing precise
equality of uncertain values should cause type errors. We believe this can help
developers be aware of noise and hence would avoid precise equality or bit-wise
operations over sensor readings.

\def\fCenter{\ \ensuremath{\vdash}\ }

\begin{prooftree}
    \Axiom$\Env \fCenter e : \texttt{NType}$
    \UnaryInf$\Env \fCenter e : \uncertain{NType}$
\end{prooftree}
\begin{prooftree}
    \Axiom$\Env \fCenter e : \uncertain{Position}$
    \UnaryInf$\Env \fCenter \posX(e) : \uncertain{int},$
    \noLine
    \UnaryInf$\Env \fCenter \posY(e) : \uncertain{int}$
\end{prooftree}
\begin{prooftree}
    \Axiom$\Env \fCenter e_1 : \uncertain{T}$
    \Axiom$\Env \fCenter e_2 : \uncertain{T}$
    \BinaryInf$\Env \fCenter e_1 \oplus e_2 : \uncertain{T}$
\end{prooftree}
\begin{prooftree}
    \Axiom$\Env \fCenter e_1 : \uncertain{T}$
    \Axiom$\Env \fCenter e_2 : \uncertain{T}$
    \BinaryInf$\Env \fCenter e_1 \bowtie e_2 : \uncertain{bool}$
\end{prooftree}
\begin{prooftree}
    \Axiom$\Env \fCenter e : \uncertain{bool}$
    \UnaryInf$\Env \fCenter \texttt{!} e : \uncertain{bool}$
\end{prooftree}
\begin{prooftree}
    \Axiom$\Env \fCenter e_1 : \uncertain{bool},$
    \noLine
    \UnaryInf$\Env \fCenter e_2 : \uncertain{bool}$
    \UnaryInf$\Env \fCenter e_1 \diamondsuit e_2 : \uncertain{bool}$
\end{prooftree}

\paragraph{Operational Semantics}

\newcommand{\REnv}{\ensuremath{\sigma}\xspace}

We now define executable semantics of our dynamic analysis. The main focus
here is to construct Bayesian network during the execution and conduct
hypothesis testing at conditionals.

\todoblock{
Define semantics that constructs Bayesian networks along a particular
execution path. One should refer to original \Koord semantics and the symbolic
semantics in Section 4.3 of \PASSERT~\cite{sampson_expressing_2014} and make
necessary adjustments.
}

\paragraph{Unresolved Problem}
For only one execution path, it is unnecessary to specify \texttt{merge}
function in \PASSERT to handle both branches since we are not doing
verification. We should be able to  skip it and further simplify expression
tree by using conjunction instead of ITE expressions so that we can construct
typical Bayesian network without conditions.

\todoblock{
Define semantics to conduct hypothesis testing at branches and preconditions to
decide which branch or event to take. There should be a black-box function that
decides whether true or false branch is taken, and we will talk about this
function in next section.
}

\paragraph{Unresolved Problem}
In \Koord, non-determinism occurs because preconditions of different events may
not be disjoint, and hence multiple events can be enabled at the same time.
This should be a design decision on how to model choice between more than one
enabled events.

\paragraph{Notes}
In Ritwika's simulation for \Koord, a deterministic scheduling is assumed that
follows the order of declaration of the events. That is, when two or more
events are enabled, it always chooses the one declared first in the given
\Koord program.

\subsection{Statistical Hypothesis Testing}

As mentioned in previous section, our executable semantics conducts hypothesis
tests at conditional to estimate the probability of taking true or false
branch.
\paragraph{Static Approach}
Here we discuss how to derive required sample size to achieve desirable
estimation error for a particular Bayesian network. Similar to \PASSERT, we can
compute Chernoffbound~\cite{chernoff1952} for a given error rate and confidence
level.

\todoblock{
Given a path condition at a certain branch, describe how to pre-compute
required sample size. There are two key problems we discussed but haven't
sorted out the detail.
}
\paragraph{Unresolved Problem 1}
Note that we will have to estimate the probability of taking true/false branch
under the condition that \textbf{along exactly the same execution path}.
Therefore, we'll have to make sure all samples drawn will follow the exact path.
That is, we have to draw samples satisfying current path condition and see how
many of them support the hypothesis.

\paragraph{Unresolved Problem 2}
Note that if we compute the bound before our analysis, we have to look up which
bound for the current path. We can start from na\"ive approach that enumerates
all path conditions and compute the bound for each path up to certain branch.
The question is how to improve this na\"ive approach.

\paragraph{Lesson Learned} Be careful that for a if-condition, there should be
two tests, one for testing true branch is taken and another for false-branch is
taken. Both tests can fail at the same time because of the required error rate
or confidence level. In that case, the analysis should be inconclusive.


\todoblock{
Discuss whether we can use techniques that dynamically decide sample size such
as Sequential probability ratio test(SPRT)~\cite{wald1945}. We originally tried
SPRT but eventually figured out that my implementation is incorrect because I
did not consider conditional probability aforementioned.
}
