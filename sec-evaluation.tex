\section{Evaluation}

In this section, we study two controller programs implementing algorithms
to stabilize state variables. For each program, we execute both original
code and our modified version and simulate with the black-box continuous
dynamics provided in the simulation framework. We then collect experiment
results for both version in order to answer following research questions.

\begin{enumerate}[label=\textbf{RQ\arabic*}]
\item\label{RQ-eff} Is our analysis more efficient than simulation in detecting
    unstable execution paths?
\item\label{RQ-oh} How much overhead~(e.g., memory usage) to run our analysis
    than to run simulation?
\end{enumerate}

\todoblock{
Design experiment to answer the research questions. The paragraph below as an
example,

To answer \ref{RQ-eff}, we execute code with and without {\tt uncertain}
annotation five times, and each time we stop at assertion violation or 1000
iterations of control loop whichever happens first. We also record time and
memory usage of each execution to estimate \ref{RQ-oh}.
}

\subsection{Case 1: HVAC}

\todoblock{Put experiment data and discuss in more detail. We should already
mentioned this example in Section~\ref{sec:example}, so the description of the
example is skipped.}

\subsection{Case 2: Line Form}

This example is extracted from the Koord paper. The goal of this program is to
align all N robots to form a line. We use the default \iRobots settings in
\Koord simulation framework for this experiment. The built-in motion controller
in \Koord is used to reach a certain GPS position when given a target position
value. The controller can be considered as an automaton that controls the
velocity and facing direction of the \iRobots. The uncertainty therefore can
come from three different kind of sensor values, namely position, facing
direction, and velocity.

\todoblock{This may not be a suitable subjects because it involves multiple
agents therefore concurrency issues. If we can find a subject that only
controls one robot, that will be easier.}

\subsection{Threats to Validity}

In this work, we assume that translated Java programs for simulation conforms
to original \Koord program semantics. However, several implementation details
of our framework can cause inconsistency between simulation and \Koord model.
\begin{itemize}
\item In \Koord model, an assumption is that all agents are synchronized. That
is, all agents transit from program turn to environment turn (and vice versa)
at the same time. However, each agent is simulated with a thread in Java
without  synchronization. It is highly possible that agents are in different
turns after more iterations.

\item We use \texttt{sleep($\delta$)} function in a infinite loop to simulate
that discrete transition happens every $\delta$-time. This is based on the
assumption that its execution time is negligible compared with $\delta$.
However, the execution time of discrete transitions will be accumulated every
iteration, and therefore the period time becomes much less precise after more
iterations.

\item \Koord language assumes the value of shared variables is consistent for
all agents. However, the agents communicate with each other through message
passing in the simulation framework. It is non-trivial to verify if the message
passing correctly implements \emph{distributed shared memory}~(DSM) systems to
ensure the consistency. Currently, \KoordFE did not implement shared memory
following any particular algorithm; hence, the correctness of the shared
variable requires further examination.

\item As mentioned above, we assume continuous distributions to model noises
for GPS sensors. Meanwhile, the implementation of GPS library is of fixed
precision and therefore discrete. We didn't analyze how these rounding errors
might affect our experiment.
\end{itemize}

In addition, our extension for uncertain data type also introduces unrealistic
program behavior due to hypothesis tests. As we discussed in
Section~\ref{sec:approach}, hypothesis tests is achieved through acquiring
indefinitely many samples of sensor readings. This assumes that each reading of
the same sensor is freshly sampled. However, it is necessary to wait for a
sampling period of the sensor to acquire the next sample in the real black-box
dynamics. Let environment turn time interval be $\delta_E$ and sensor sampling
period
be $\delta_S$, we can therefore consider following two possible scenarios.
\begin{enumerate}
\item $\delta_S \ll \delta_E$

If the sampling period $\delta_S$ is much smaller than environment turn time
interval $\delta_E$, we can then assume that all samples are acquired within
one  $\delta_E$, and hence waiting for samples is no longer a problem. However,
it is most likely unrealistic as environment turn should be defined by some
sampling periods according to \Koord semantics.

\item $\delta_S \not\ll \delta_E$

In this scenario, we'd have to acquire samples via multiple environment
turn transitions. This poses a challenge that the environment may have already
changed, and therefore later sensor readings are no longer for the same
environment state as the previous readings. This is not consistent with our
assumption that samples are from one environment state with noisy sensors.

\end{enumerate}