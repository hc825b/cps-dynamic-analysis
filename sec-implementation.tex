\section{Implementation}

\begin{figure*}[ht]
    \includegraphics[width=\textwidth]{koord-arch.png}
    \caption{Architecture of simulation framework for \Koord}
    \label{fig:koord-arch}
\end{figure*}

We implement our approach via extending the simulation framework for \Koord.
According to \Koord semantics, each agent can be represented as an automaton.
The basic concept of \KoordFE is to translate a \Koord agent to a StarL
application (implemented in Java thread) that simulates a automaton.
From Figure~\ref{fig:koord-arch}, we can see that, to simulate a given \Koord
program, currently it is translated to a Java program via \KoordFE (denoted as
Parser), and then it is compiled and executed with external Java libraries
(denoted as Physical Environment) that provide sensor readings from such as
simulated \iRobots or drones.

To realize our approach on \Koord, we first modify \KoordFE to accept
\texttt{uncertain} qualifiers on top of \Koord programming language so that
these annotations can instruct \KoordFE to use our Java library for uncertain
variables. We skip description for this component as it is purely syntactic
transformation.
We then need a data structure of directed acyclic graph to represent Bayesian
networks. A major design concern is that it should be constructed when executing
translated \Koord program following our executable semantics. Our implementation
is largely inspired by \cite{bornholt_uncertain<t>:_2014} except we implement
in Java.

The implementation can be found on GitHub repository~\footnote{
\url{https://github.com/hc825b/Koord-Front-End}}.
In the following sections, we will first briefly sketch how Bayesian network is
implemented as Java classes in Section~\ref{subsec:network}, and later explain
we design sampling from black-box continuous dynamics to approximate probability
distribution in Section~\ref{subsec:sampling}.

\paragraph{Unresolved Problem}
In \Koord simulation, global variables sharing across agents (Java threads) and
environment (simulated by another thread) are used to simulate synchronization,
so even with only one agent the variables are still shared by more than one
thread. However, implementation described in this section did not consider
concurrency at all. The correctness of the analysis can well be questioned.


\subsection{Bayesian Network}\label{subsec:network}

Our main implementation starts from providing uncertain versions of supported
data types in Java. Currently, we support \Integer and \Float. As discussed in
Section~\ref{sec:approach}, each expression over uncertain variables is
conceptually a Bayesian network. Each leaf node in the network is a primitive
probabilistic distribution that models the noise of reading a sensor value, and
each internal node is an arithmetic or logical operator over sub-networks.

To achieve this in Java, our implementation simply provides an recursive data
structure, {\tt DAG}, as base type and use different sub-types to support leaf
node and internal node for each kind of operator. A particular leaf node type
is {\tt LeafBlackBox}. It accepts an {\tt Supplier} function as the sampling
function. This allows the developer to provide customized sampling function for
constructed Bayesian network.

Each {\tt DAG} provides an {\tt eval} function to evaluate the value of current
network. For internal operator nodes, it calculates the value by applying the
operator on the result of {\tt eval} from sub-network accordingly.
For {\tt LeafBlackBox} nodes, {\tt eval} will draw a sample from the provided
{\tt Supplier} function. Because the whole network is a DAG, it is important to
avoid drawing samples repeatedly from the same leaf node visited multiple
times. In~\cite{bornholt_uncertain<t>:_2014}, they do topological sort and
decide the order of evaluation beforehand to avoid repetition. In our
implementation however, we simply add a guard before sampling {\tt
LeafBlackBox} nodes for quick prototyping.

On top of {\tt DAG} class, we provide a generic {\tt Uncertain} class to
wrap graph composition according to given operators. Two derived types from
{\tt Uncertain} type are {\tt Primitive} class and {\ Compound} class. As the
name indicates, {\tt Primitive} ({\tt Compound}) type represents primitive
(compound) distributions. A new {\tt Primitive} instance creates a new leaf
node in the network. Similarly, {\tt Compound} instance introduce an internal
node. Note that {\tt Compound} type is not accessible from outside of {\tt
Uncertain} class. User can only indirectly use it via provided methods. This
helps ensure that the constructed DAG is acyclic.

In addition to above Java classes, we provide a set of wrapper functions that
overloads all possible combinations of types to simplify the translation of
arithmetic and logical operators. For logical OR operator as an example, we
provide following four overloaded versions of function signature.

\begin{itemize}
\item {\tt opOr(bool, bool)}
\item {\tt opOr(Uncertain<Boolean>, bool)}
\item {\tt opOr(bool, Uncertain<Boolean>)}
\item {\small \tt opOr(Uncertain<Boolean>, Uncertain<Boolean>)}
\end{itemize}

For example, when translating programs from \Koord to Java, we can simply
replace expression {\tt x || y} with {\tt opOr(x, y)} without worrying about
mixing certain and uncertain types. Further, if {\tt x} and {\tt y} are of
incompatible types, e.g., {\tt x} is {\tt int} instead of {\tt bool} type, Java
compiler will produce compilation errors so that we know the given \Koord
program is not well-typed.

\subsection{Sampling}\label{subsec:sampling}

In addition to obtaining sensor readings from black-box continuous dynamics, it
is also important to properly define the assumptions over distributions.

\todoblock{Discuss what is the black-box distribution we are drawing from when
reading from a sensor.

}

\paragraph{Lesson Learned}
It is important to know if the precision of a given sensor is $\sigma$, it is
normally interpreted or approximated as following
\[
    Pr[Reading | Env = v] = Gaussian(v, \sigma^2)
\]
In other words, the distribution of sensor readings over true environment state
(a reference value $v$) is modeled by a normal distribution center at reference
value.
See \href{https://en.wikipedia.org/wiki/Accuracy_and_precision}{Wikipedia page
of Accuracy and Precision} for definition.

First appraoch is that drawing multiple readings can be considered as sampling
from  $Pr[Reading]$ directly (under the assumption that environment state does
not change when we are sampling). And we'd have to derive $Pr[Env]$ as the
sampling function for the leaf node in Bayesian network.

Another approach is proposed in \UncertainT. They made several assumption so
that they can pre-compute the PDF of $Pr[Env|Reading = s]$ (for GPS). When they
obtain a GPS sensor reading $s$, they draw samples from $Pr[Env|Reading = s]$
with the concrete $s$ and conduct the hypothesis tests using SPRT.

\paragraph{Technical Difficulties}{As mentioned before, the sample size for now
is dynamically determined with SPRT. To change to fix number of samples will be
easy. The difficulty comes from calculating sample size beforehand for all
possible paths. It seems laborious to me to enumerate all paths in
\textbf{\Koord program} (not in Java) and construct Bayesian network for each
path.}
