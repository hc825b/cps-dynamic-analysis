\section{Introduction}

Existing hybrid system verification approaches often relies on the availability
of nice mathematical models describing the behavior of physical world. In
practice, these ``models'' however are typically a mixture of simulation code,
differential equations, block diagrams, or look-up tables collected from
experiments. To extract unified mathematical representations from these
heterogeneous descriptions is most likely infeasible. On the other hand,
simulation and field testing are widely applied in control engineering to look
for correctness or robustness issues without clear model assumptions. In
particular, Robotics and AI communities already start to provide frameworks
enabling more realistic simulation such as Gazebo~\cite{Koenig04designand},
AirSim~\cite{airsim2017fsr}, etc. These frameworks normally come with
programmable interfaces to obtain traces from simulated environments for given
controller implementations, but are difficult to represent as mathematical
models. Under these conditions, most control system development share this
combination of white-box controller design and black-box simulated environment
as pointed out by Fan et al. in \cite{fan_dryvr:_2017}. Motivated by this
observation, we aim to explore program analysis techniques with black-box
simulation to derive safety or robustness guarantees for the white-box
controller.

In this work, we specifically target robustness guarantees against
\emph{perception uncertainty}~\cite{Thrun:2005:PR:1121596} from black-box
environment. Perception
uncertainty mainly arises from errors of state estimations. These errors can be
caused by limits of hardware precision as well as the inherent limitations of
state estimation algorithms. Hence, the estimated environment states are often
represented in the form of probabilistic beliefs~\cite{Thrun:2005:PR:1121596}.
For example,
Simultaneous Localization and Mapping (SLAM)~\cite{bailey_simultaneous_2006}, is
a popular technique used to determine the current position of an autonomous
vehicle. The estimated positions of the vehicle are often assumed to have
Gaussian noise. We argue that obtaining readings from black-box simulated
environment can be considered as sampling from assumed probability
distributions, and therefore it is of most interest to verify against
probabilistic correctness guarantees using black-box simulation with statistics.

Our goal is to devise a dynamic analysis to examine if a given controller
implementation can violate probabilistic correctness properties with a black-box
simulation framework.
